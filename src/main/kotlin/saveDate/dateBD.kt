package saveDate

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate


object Dates: Table(){
    val id: Column<Int> = integer("id").autoIncrement()
    val data: Column<String> = varchar("date", length = 20)
    override val primaryKey = PrimaryKey(id)
}

fun main(args: Array<String>){
    var localDate = LocalDate.now().toString()

    Database.connect("jdbc:h2:./myh2File", driver = "org.h2.Driver")

    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Dates)

        Dates.insert{
            it[data] = localDate
        }
        println("Date: ${Dates.selectAll()}")

        Dates.selectAll().onEach {
            println(it)
        }
    }


}