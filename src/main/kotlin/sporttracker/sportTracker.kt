package sporttracker

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.transactions.transaction
import sporttracker.Esports.esport
import java.util.Scanner

object Esports: Table(){
    private val id:Column<Int> = integer("id").autoIncrement()
    val esport:Column<String> =varchar("esport", length = 100)
    val temps:Column<Int> = integer("temps")
    override val primaryKey = PrimaryKey(id)
}

fun main(args: Array<String>){
    val scanner = Scanner(System.`in`)

    println("Quin esport has practicat?")
    val sport:String = scanner.next()
    println("Quin temps has dedicat? <int allowed>")
    val time:Int = scanner.nextInt()

    Database.connect("jdbc:h2:./sportTrackerFile", driver = "org.h2.Driver")

    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Esports)

        val query: Query = Esports.select(esport eq sport)

        if(query.empty()){
            Esports.insert {
                it[esport] = sport
                it[temps] = time
            }
        }else{
            Esports.update({esport eq sport}){
                with(SqlExpressionBuilder){
                    it.update(temps, temps + time)
                }
            }
        }

        Esports.selectAll().onEach {
            println(it)
        }
    }

}


