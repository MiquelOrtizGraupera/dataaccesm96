package nba

import nba.Players.name
import nba.Players.nameiteam
import nba.Teams.name
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.like
import org.jetbrains.exposed.sql.transactions.transaction

object Teams: Table(){
    val name:Column<String> = varchar("name", length = 20)
    val city:Column<String> = varchar("city", length = 20)
    val conference:Column<String> = varchar("conference", length = 4)
    val division:Column<String> = varchar("division", length = 9)
    override val primaryKey = PrimaryKey(name)
}

object Players:Table(){
    val id:Column<Int> = integer("id")
    val name:Column<String> = varchar("name", length = 30)
    val origin:Column<String> = varchar("origin", length = 20)
    val height:Column<String> = varchar("height", length = 4)
    val weight:Column<Int> = integer("weight")
    val position:Column<String> = varchar("position", length = 12)
    val nameiteam:Column<String> = varchar("nameiteam", length = 20)
    override val primaryKey = PrimaryKey(id)

}
fun main(args: Array<String>){

    val conexion:Database = Database.connect("jdbc:postgresql://localhost:5432/postgres", driver = "org.postgresql.Driver",
        user = "sjo")
    //First exercice
    getAllTeamNames(conexion)
    //Second Exercice
   getPlayerByNameAndReturnCity(conexion)

}

private fun getAllTeamNames(conexion:Database){
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Teams)


        Teams.selectAll().onEach {
            print(it[Teams.name]+" ")
        }
        println()
    }
}

private fun getPlayerByNameAndReturnCity(conexion: Database){
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(Players)

        Players.selectAll().andWhere { Players.name like "Pau Gasol" }.forEach { println(it[nameiteam]) }
    }
}